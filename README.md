Simple Hangman Game 


1. Introduction

This project is a very simple hangman game which I created early in my programming journey.
This project was created for an assignment in the Event Driven Programming module in during my Level 3 in IT.

2. Features

I used C# and Windows Forms to create a game with 3 levels of difficulty.
The game reads in these words from a .txt file.
The app clearly shows the letters the user has guessed and how far through the hangman is to being completed.
There are on-screen help button and an admin section for the words in the database to be edited.

3. Installation

Clone the repository
Run from Assignment/bin/Debug/Assignment.exe

