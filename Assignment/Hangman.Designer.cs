﻿namespace Assignment
{
    partial class Hangman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hangman));
            this.lblOne = new System.Windows.Forms.Label();
            this.lblTwo = new System.Windows.Forms.Label();
            this.lblThree = new System.Windows.Forms.Label();
            this.lblFour = new System.Windows.Forms.Label();
            this.lblFive = new System.Windows.Forms.Label();
            this.txtAnswer = new System.Windows.Forms.TextBox();
            this.btnEnter = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.lbxUsed = new System.Windows.Forms.ListBox();
            this.pbxHang2 = new System.Windows.Forms.PictureBox();
            this.pbxHang3 = new System.Windows.Forms.PictureBox();
            this.pbxHang5 = new System.Windows.Forms.PictureBox();
            this.pbxHang7 = new System.Windows.Forms.PictureBox();
            this.lblWrongLetters = new System.Windows.Forms.Label();
            this.pbxHang1 = new System.Windows.Forms.PictureBox();
            this.pbxHang4 = new System.Windows.Forms.PictureBox();
            this.pbxHang6 = new System.Windows.Forms.PictureBox();
            this.btnHangHelp = new System.Windows.Forms.Button();
            this.pbxKids = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxKids)).BeginInit();
            this.SuspendLayout();
            // 
            // lblOne
            // 
            this.lblOne.AutoSize = true;
            this.lblOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOne.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblOne.Location = new System.Drawing.Point(88, 110);
            this.lblOne.Name = "lblOne";
            this.lblOne.Size = new System.Drawing.Size(26, 29);
            this.lblOne.TabIndex = 0;
            this.lblOne.Text = "_";
            // 
            // lblTwo
            // 
            this.lblTwo.AutoSize = true;
            this.lblTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTwo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblTwo.Location = new System.Drawing.Point(190, 110);
            this.lblTwo.Name = "lblTwo";
            this.lblTwo.Size = new System.Drawing.Size(26, 29);
            this.lblTwo.TabIndex = 1;
            this.lblTwo.Text = "_";
            // 
            // lblThree
            // 
            this.lblThree.AutoSize = true;
            this.lblThree.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThree.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblThree.Location = new System.Drawing.Point(285, 110);
            this.lblThree.Name = "lblThree";
            this.lblThree.Size = new System.Drawing.Size(26, 29);
            this.lblThree.TabIndex = 2;
            this.lblThree.Text = "_";
            // 
            // lblFour
            // 
            this.lblFour.AutoSize = true;
            this.lblFour.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFour.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblFour.Location = new System.Drawing.Point(385, 110);
            this.lblFour.Name = "lblFour";
            this.lblFour.Size = new System.Drawing.Size(26, 29);
            this.lblFour.TabIndex = 3;
            this.lblFour.Text = "_";
            // 
            // lblFive
            // 
            this.lblFive.AutoSize = true;
            this.lblFive.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFive.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblFive.Location = new System.Drawing.Point(479, 110);
            this.lblFive.Name = "lblFive";
            this.lblFive.Size = new System.Drawing.Size(26, 29);
            this.lblFive.TabIndex = 4;
            this.lblFive.Text = "_";
            // 
            // txtAnswer
            // 
            this.txtAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnswer.Location = new System.Drawing.Point(44, 49);
            this.txtAnswer.MaxLength = 1;
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(481, 34);
            this.txtAnswer.TabIndex = 5;
            this.txtAnswer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAnswer_KeyDown);
            // 
            // btnEnter
            // 
            this.btnEnter.BackColor = System.Drawing.Color.Red;
            this.btnEnter.ForeColor = System.Drawing.Color.White;
            this.btnEnter.Location = new System.Drawing.Point(595, 49);
            this.btnEnter.Name = "btnEnter";
            this.btnEnter.Size = new System.Drawing.Size(157, 34);
            this.btnEnter.TabIndex = 6;
            this.btnEnter.Text = "Enter Letter";
            this.btnEnter.UseVisualStyleBackColor = false;
            this.btnEnter.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.Lime;
            this.btnNext.Location = new System.Drawing.Point(575, 339);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(182, 64);
            this.btnNext.TabIndex = 7;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lbxUsed
            // 
            this.lbxUsed.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxUsed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbxUsed.FormattingEnabled = true;
            this.lbxUsed.ItemHeight = 29;
            this.lbxUsed.Location = new System.Drawing.Point(590, 136);
            this.lbxUsed.Name = "lbxUsed";
            this.lbxUsed.Size = new System.Drawing.Size(162, 178);
            this.lbxUsed.TabIndex = 8;
            // 
            // pbxHang2
            // 
            this.pbxHang2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang2.BackgroundImage")));
            this.pbxHang2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang2.Location = new System.Drawing.Point(44, 176);
            this.pbxHang2.Name = "pbxHang2";
            this.pbxHang2.Size = new System.Drawing.Size(481, 227);
            this.pbxHang2.TabIndex = 9;
            this.pbxHang2.TabStop = false;
            // 
            // pbxHang3
            // 
            this.pbxHang3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang3.BackgroundImage")));
            this.pbxHang3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang3.Location = new System.Drawing.Point(44, 176);
            this.pbxHang3.Name = "pbxHang3";
            this.pbxHang3.Size = new System.Drawing.Size(481, 227);
            this.pbxHang3.TabIndex = 10;
            this.pbxHang3.TabStop = false;
            // 
            // pbxHang5
            // 
            this.pbxHang5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang5.BackgroundImage")));
            this.pbxHang5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang5.Location = new System.Drawing.Point(44, 176);
            this.pbxHang5.Name = "pbxHang5";
            this.pbxHang5.Size = new System.Drawing.Size(481, 227);
            this.pbxHang5.TabIndex = 11;
            this.pbxHang5.TabStop = false;
            // 
            // pbxHang7
            // 
            this.pbxHang7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang7.BackgroundImage")));
            this.pbxHang7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang7.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pbxHang7.ErrorImage")));
            this.pbxHang7.Location = new System.Drawing.Point(44, 164);
            this.pbxHang7.Name = "pbxHang7";
            this.pbxHang7.Size = new System.Drawing.Size(481, 227);
            this.pbxHang7.TabIndex = 12;
            this.pbxHang7.TabStop = false;
            // 
            // lblWrongLetters
            // 
            this.lblWrongLetters.AutoSize = true;
            this.lblWrongLetters.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lblWrongLetters.Location = new System.Drawing.Point(592, 116);
            this.lblWrongLetters.Name = "lblWrongLetters";
            this.lblWrongLetters.Size = new System.Drawing.Size(123, 17);
            this.lblWrongLetters.TabIndex = 13;
            this.lblWrongLetters.Text = "Incorrect Guesses";
            // 
            // pbxHang1
            // 
            this.pbxHang1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang1.BackgroundImage")));
            this.pbxHang1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang1.Location = new System.Drawing.Point(44, 176);
            this.pbxHang1.Name = "pbxHang1";
            this.pbxHang1.Size = new System.Drawing.Size(481, 227);
            this.pbxHang1.TabIndex = 14;
            this.pbxHang1.TabStop = false;
            // 
            // pbxHang4
            // 
            this.pbxHang4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang4.BackgroundImage")));
            this.pbxHang4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang4.Location = new System.Drawing.Point(44, 176);
            this.pbxHang4.Name = "pbxHang4";
            this.pbxHang4.Size = new System.Drawing.Size(481, 227);
            this.pbxHang4.TabIndex = 15;
            this.pbxHang4.TabStop = false;
            // 
            // pbxHang6
            // 
            this.pbxHang6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxHang6.BackgroundImage")));
            this.pbxHang6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxHang6.Location = new System.Drawing.Point(44, 176);
            this.pbxHang6.Name = "pbxHang6";
            this.pbxHang6.Size = new System.Drawing.Size(481, 227);
            this.pbxHang6.TabIndex = 16;
            this.pbxHang6.TabStop = false;
            // 
            // btnHangHelp
            // 
            this.btnHangHelp.BackColor = System.Drawing.Color.Blue;
            this.btnHangHelp.ForeColor = System.Drawing.Color.White;
            this.btnHangHelp.Location = new System.Drawing.Point(575, 425);
            this.btnHangHelp.Name = "btnHangHelp";
            this.btnHangHelp.Size = new System.Drawing.Size(182, 64);
            this.btnHangHelp.TabIndex = 17;
            this.btnHangHelp.Text = "Help";
            this.btnHangHelp.UseVisualStyleBackColor = false;
            this.btnHangHelp.Click += new System.EventHandler(this.btnHangHelp_Click);
            // 
            // pbxKids
            // 
            this.pbxKids.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxKids.BackgroundImage")));
            this.pbxKids.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxKids.Location = new System.Drawing.Point(44, 413);
            this.pbxKids.Name = "pbxKids";
            this.pbxKids.Size = new System.Drawing.Size(481, 146);
            this.pbxKids.TabIndex = 18;
            this.pbxKids.TabStop = false;
            // 
            // Hangman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(774, 571);
            this.Controls.Add(this.pbxKids);
            this.Controls.Add(this.btnHangHelp);
            this.Controls.Add(this.pbxHang7);
            this.Controls.Add(this.pbxHang6);
            this.Controls.Add(this.pbxHang5);
            this.Controls.Add(this.pbxHang4);
            this.Controls.Add(this.lblWrongLetters);
            this.Controls.Add(this.pbxHang3);
            this.Controls.Add(this.pbxHang2);
            this.Controls.Add(this.lbxUsed);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnEnter);
            this.Controls.Add(this.txtAnswer);
            this.Controls.Add(this.lblFive);
            this.Controls.Add(this.lblFour);
            this.Controls.Add(this.lblThree);
            this.Controls.Add(this.lblTwo);
            this.Controls.Add(this.lblOne);
            this.Controls.Add(this.pbxHang1);
            this.Name = "Hangman";
            this.Text = "Hangman";
            this.Load += new System.EventHandler(this.Hangman_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Hangman_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHang6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxKids)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblOne;
        private System.Windows.Forms.Label lblTwo;
        private System.Windows.Forms.Label lblThree;
        private System.Windows.Forms.Label lblFour;
        private System.Windows.Forms.Label lblFive;
        private System.Windows.Forms.TextBox txtAnswer;
        private System.Windows.Forms.Button btnEnter;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.ListBox lbxUsed;
        private System.Windows.Forms.PictureBox pbxHang2;
        private System.Windows.Forms.PictureBox pbxHang3;
        private System.Windows.Forms.PictureBox pbxHang5;
        private System.Windows.Forms.PictureBox pbxHang7;
        private System.Windows.Forms.Label lblWrongLetters;
        private System.Windows.Forms.PictureBox pbxHang1;
        private System.Windows.Forms.PictureBox pbxHang4;
        private System.Windows.Forms.PictureBox pbxHang6;
        private System.Windows.Forms.Button btnHangHelp;
        private System.Windows.Forms.PictureBox pbxKids;
    }
}