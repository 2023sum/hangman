﻿namespace Assignment
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.btnLevel1 = new System.Windows.Forms.Button();
            this.btnLevel2 = new System.Windows.Forms.Button();
            this.btnLevel3 = new System.Windows.Forms.Button();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.btnMenuHelp = new System.Windows.Forms.Button();
            this.pbxSpongebob = new System.Windows.Forms.PictureBox();
            this.pbxYoda = new System.Windows.Forms.PictureBox();
            this.pbxBarry = new System.Windows.Forms.PictureBox();
            this.pbxMatt = new System.Windows.Forms.PictureBox();
            this.pbxDenzel = new System.Windows.Forms.PictureBox();
            this.pbxLincoln = new System.Windows.Forms.PictureBox();
            this.pbxShrek = new System.Windows.Forms.PictureBox();
            this.pbxDog = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSpongebob)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxYoda)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBarry)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMatt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDenzel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLincoln)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxShrek)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDog)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLevel1
            // 
            this.btnLevel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btnLevel1.ForeColor = System.Drawing.Color.White;
            this.btnLevel1.Location = new System.Drawing.Point(192, 94);
            this.btnLevel1.Name = "btnLevel1";
            this.btnLevel1.Size = new System.Drawing.Size(416, 48);
            this.btnLevel1.TabIndex = 0;
            this.btnLevel1.Text = "Level 1";
            this.btnLevel1.UseVisualStyleBackColor = false;
            this.btnLevel1.Click += new System.EventHandler(this.btnLevel1_Click);
            // 
            // btnLevel2
            // 
            this.btnLevel2.BackColor = System.Drawing.Color.Lime;
            this.btnLevel2.Location = new System.Drawing.Point(192, 201);
            this.btnLevel2.Name = "btnLevel2";
            this.btnLevel2.Size = new System.Drawing.Size(416, 48);
            this.btnLevel2.TabIndex = 1;
            this.btnLevel2.Text = "Level 2";
            this.btnLevel2.UseVisualStyleBackColor = false;
            this.btnLevel2.Click += new System.EventHandler(this.btnLevel2_Click);
            // 
            // btnLevel3
            // 
            this.btnLevel3.BackColor = System.Drawing.Color.Purple;
            this.btnLevel3.ForeColor = System.Drawing.Color.White;
            this.btnLevel3.Location = new System.Drawing.Point(192, 308);
            this.btnLevel3.Name = "btnLevel3";
            this.btnLevel3.Size = new System.Drawing.Size(416, 48);
            this.btnLevel3.TabIndex = 2;
            this.btnLevel3.Text = "Level 3";
            this.btnLevel3.UseVisualStyleBackColor = false;
            this.btnLevel3.Click += new System.EventHandler(this.btnLevel3_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.BackColor = System.Drawing.Color.Red;
            this.btnAdmin.Location = new System.Drawing.Point(4, 7);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(147, 32);
            this.btnAdmin.TabIndex = 3;
            this.btnAdmin.Text = "ADMIN";
            this.btnAdmin.UseVisualStyleBackColor = false;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // btnMenuHelp
            // 
            this.btnMenuHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnMenuHelp.Location = new System.Drawing.Point(650, 7);
            this.btnMenuHelp.Name = "btnMenuHelp";
            this.btnMenuHelp.Size = new System.Drawing.Size(138, 38);
            this.btnMenuHelp.TabIndex = 4;
            this.btnMenuHelp.Text = "Help";
            this.btnMenuHelp.UseVisualStyleBackColor = false;
            this.btnMenuHelp.Click += new System.EventHandler(this.btnMenuHelp_Click);
            // 
            // pbxSpongebob
            // 
            this.pbxSpongebob.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxSpongebob.BackgroundImage")));
            this.pbxSpongebob.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxSpongebob.Location = new System.Drawing.Point(4, 290);
            this.pbxSpongebob.Name = "pbxSpongebob";
            this.pbxSpongebob.Size = new System.Drawing.Size(137, 159);
            this.pbxSpongebob.TabIndex = 5;
            this.pbxSpongebob.TabStop = false;
            // 
            // pbxYoda
            // 
            this.pbxYoda.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxYoda.BackgroundImage")));
            this.pbxYoda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxYoda.Location = new System.Drawing.Point(130, 398);
            this.pbxYoda.Name = "pbxYoda";
            this.pbxYoda.Size = new System.Drawing.Size(230, 143);
            this.pbxYoda.TabIndex = 6;
            this.pbxYoda.TabStop = false;
            // 
            // pbxBarry
            // 
            this.pbxBarry.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxBarry.BackgroundImage")));
            this.pbxBarry.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxBarry.Location = new System.Drawing.Point(635, 169);
            this.pbxBarry.Name = "pbxBarry";
            this.pbxBarry.Size = new System.Drawing.Size(153, 217);
            this.pbxBarry.TabIndex = 8;
            this.pbxBarry.TabStop = false;
            // 
            // pbxMatt
            // 
            this.pbxMatt.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxMatt.BackgroundImage")));
            this.pbxMatt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxMatt.Location = new System.Drawing.Point(0, 455);
            this.pbxMatt.Name = "pbxMatt";
            this.pbxMatt.Size = new System.Drawing.Size(124, 103);
            this.pbxMatt.TabIndex = 10;
            this.pbxMatt.TabStop = false;
            // 
            // pbxDenzel
            // 
            this.pbxDenzel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxDenzel.BackgroundImage")));
            this.pbxDenzel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxDenzel.ErrorImage = null;
            this.pbxDenzel.Location = new System.Drawing.Point(589, 392);
            this.pbxDenzel.Name = "pbxDenzel";
            this.pbxDenzel.Size = new System.Drawing.Size(241, 166);
            this.pbxDenzel.TabIndex = 11;
            this.pbxDenzel.TabStop = false;
            // 
            // pbxLincoln
            // 
            this.pbxLincoln.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxLincoln.BackgroundImage")));
            this.pbxLincoln.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxLincoln.Location = new System.Drawing.Point(624, 58);
            this.pbxLincoln.Name = "pbxLincoln";
            this.pbxLincoln.Size = new System.Drawing.Size(112, 113);
            this.pbxLincoln.TabIndex = 12;
            this.pbxLincoln.TabStop = false;
            // 
            // pbxShrek
            // 
            this.pbxShrek.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxShrek.BackgroundImage")));
            this.pbxShrek.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxShrek.Location = new System.Drawing.Point(4, 94);
            this.pbxShrek.Name = "pbxShrek";
            this.pbxShrek.Size = new System.Drawing.Size(182, 176);
            this.pbxShrek.TabIndex = 13;
            this.pbxShrek.TabStop = false;
            // 
            // pbxDog
            // 
            this.pbxDog.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxDog.BackgroundImage")));
            this.pbxDog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxDog.Location = new System.Drawing.Point(371, 362);
            this.pbxDog.Name = "pbxDog";
            this.pbxDog.Size = new System.Drawing.Size(202, 196);
            this.pbxDog.TabIndex = 14;
            this.pbxDog.TabStop = false;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 553);
            this.Controls.Add(this.pbxDog);
            this.Controls.Add(this.pbxShrek);
            this.Controls.Add(this.pbxLincoln);
            this.Controls.Add(this.pbxDenzel);
            this.Controls.Add(this.pbxMatt);
            this.Controls.Add(this.pbxBarry);
            this.Controls.Add(this.pbxYoda);
            this.Controls.Add(this.pbxSpongebob);
            this.Controls.Add(this.btnMenuHelp);
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.btnLevel3);
            this.Controls.Add(this.btnLevel2);
            this.Controls.Add(this.btnLevel1);
            this.Name = "MainMenu";
            this.Text = "Main Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pbxSpongebob)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxYoda)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBarry)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxMatt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDenzel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLincoln)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxShrek)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxDog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLevel1;
        private System.Windows.Forms.Button btnLevel2;
        private System.Windows.Forms.Button btnLevel3;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Button btnMenuHelp;
        private System.Windows.Forms.PictureBox pbxSpongebob;
        private System.Windows.Forms.PictureBox pbxYoda;
        private System.Windows.Forms.PictureBox pbxBarry;
        private System.Windows.Forms.PictureBox pbxMatt;
        private System.Windows.Forms.PictureBox pbxDenzel;
        private System.Windows.Forms.PictureBox pbxLincoln;
        private System.Windows.Forms.PictureBox pbxShrek;
        private System.Windows.Forms.PictureBox pbxDog;
    }
}