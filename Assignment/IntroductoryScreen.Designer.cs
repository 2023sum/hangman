﻿namespace Assignment
{
    partial class IntroductoryScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IntroductoryScreen));
            this.tmrSplash = new System.Windows.Forms.Timer(this.components);
            this.txtInfo1 = new System.Windows.Forms.TextBox();
            this.pbxCompanyLogo = new System.Windows.Forms.PictureBox();
            this.txtLabel1 = new System.Windows.Forms.Label();
            this.txtInfo2 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrSplash
            // 
            this.tmrSplash.Tick += new System.EventHandler(this.tmrSplash_Tick);
            // 
            // txtInfo1
            // 
            this.txtInfo1.Enabled = false;
            this.txtInfo1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfo1.Location = new System.Drawing.Point(90, 137);
            this.txtInfo1.Multiline = true;
            this.txtInfo1.Name = "txtInfo1";
            this.txtInfo1.Size = new System.Drawing.Size(617, 121);
            this.txtInfo1.TabIndex = 0;
            this.txtInfo1.Text = resources.GetString("txtInfo1.Text");
            this.txtInfo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pbxCompanyLogo
            // 
            this.pbxCompanyLogo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbxCompanyLogo.BackgroundImage")));
            this.pbxCompanyLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbxCompanyLogo.Location = new System.Drawing.Point(183, 12);
            this.pbxCompanyLogo.Name = "pbxCompanyLogo";
            this.pbxCompanyLogo.Size = new System.Drawing.Size(444, 119);
            this.pbxCompanyLogo.TabIndex = 1;
            this.pbxCompanyLogo.TabStop = false;
            this.pbxCompanyLogo.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // txtLabel1
            // 
            this.txtLabel1.AutoSize = true;
            this.txtLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLabel1.ForeColor = System.Drawing.Color.Teal;
            this.txtLabel1.Location = new System.Drawing.Point(279, 372);
            this.txtLabel1.Name = "txtLabel1";
            this.txtLabel1.Size = new System.Drawing.Size(245, 20);
            this.txtLabel1.TabIndex = 2;
            this.txtLabel1.Text = "Click Anywhere To Continue";
            this.txtLabel1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtInfo2
            // 
            this.txtInfo2.Enabled = false;
            this.txtInfo2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfo2.Location = new System.Drawing.Point(291, 264);
            this.txtInfo2.Multiline = true;
            this.txtInfo2.Name = "txtInfo2";
            this.txtInfo2.Size = new System.Drawing.Size(233, 78);
            this.txtInfo2.TabIndex = 3;
            this.txtInfo2.Text = "Level 1: 3 letter words Level 2 : 4 letter words Level 3 : 5 letter words";
            this.txtInfo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // IntroductoryScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtInfo2);
            this.Controls.Add(this.txtLabel1);
            this.Controls.Add(this.pbxCompanyLogo);
            this.Controls.Add(this.txtInfo1);
            this.Name = "IntroductoryScreen";
            this.Text = "Introductory Screen";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IntroductoryScreen_MouseClick);
            ((System.ComponentModel.ISupportInitialize)(this.pbxCompanyLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer tmrSplash;
        private System.Windows.Forms.TextBox txtInfo1;
        private System.Windows.Forms.PictureBox pbxCompanyLogo;
        private System.Windows.Forms.Label txtLabel1;
        private System.Windows.Forms.TextBox txtInfo2;
    }
}

