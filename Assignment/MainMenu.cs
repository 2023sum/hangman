﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Assignment
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }
        
        private void btnLevel1_Click(object sender, EventArgs e)
        {
            //Show hangman form. Give 1
            Form Hangman = new Hangman(1);
            Hangman.Show();
        }

        private void btnLevel2_Click(object sender, EventArgs e)
        {
            //Show hangman form. Give 2
            Form Hangman = new Hangman(2);
            Hangman.Show();
        }

        private void btnLevel3_Click(object sender, EventArgs e)
        {
            //Show hangman form. Give 3
            Form Hangman = new Hangman(3);
            Hangman.Show();
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            // Show text file in notepad
            System.Diagnostics.Process.Start("notepad.exe", "words.txt");
        }

        private void btnMenuHelp_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please click one of the three buttons that correspond to different levels.\n\nLevel 1 consists of 3 letter words.\nLevel 2 consists of 4 letter words.\nLevel 3 consists of 5 letter words.\n\nAdmins:\nUse the ADMIN button to make edits to the textfile containing the words used in each level of the game");                     
        }    
    }
}
