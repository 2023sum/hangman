﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment
{
    public partial class IntroductoryScreen : Form
    {
        Timer tmr; // Timer
        bool show = false;
        Form MainMenu = new MainMenu();

        public IntroductoryScreen()
        {
            InitializeComponent();

            tmr = new Timer();
            tmr.Interval = 10000; // 10 seconds
            tmr.Start(); // Start timer

            tmr.Tick += tmrSplash_Tick;
        }

        private void tmrSplash_Tick(object sender, EventArgs e)
        {
            if (show == false)
            {
                tmr.Stop(); // Stop timer           
                MainMenu.ShowDialog(); // Show main menu form
            }          
        }

        private void IntroductoryScreen_MouseClick(object sender, MouseEventArgs e)
        {
            show = true; // Stops timer from showing form          
            MainMenu.ShowDialog(); // Show main menu form
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            show = true; // Stops timer from showing form          
            MainMenu.ShowDialog(); // Show main menu form
        }

        private void label1_Click(object sender, EventArgs e)
        {
            show = true; // Stops timer from showing form  
            MainMenu.ShowDialog(); // Show main menu form
        }
    }
}
