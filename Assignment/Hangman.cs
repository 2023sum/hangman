﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Speech.Synthesis;

namespace Assignment
{
    public partial class Hangman : Form
    {
        // Stores random numbers
        List<int> ranNumbers = new List<int>();

        int number; // user level select
        int wrongCount = 0;
        int questionCount = 0;
        int paintCount = 0;

        List<String> words = new List<String>();
        List<String> letters = new List<String>();

        // Stores words in each level
        List<String> level1 = new List<String>();
        List<String> level2 = new List<String>();
        List<String> level3 = new List<String>();

        SpeechSynthesizer synth = new SpeechSynthesizer();

        public Hangman(int number)
        {
            InitializeComponent();
            this.number = number;
            synth.SetOutputToDefaultAudioDevice();        
        }
    
        int nextCount = 0;

        private void Hangman_Load(object sender, EventArgs e)
        {

            // Hides all the hangman pictures
            pbxHang1.Hide();
            pbxHang2.Hide();
            pbxHang3.Hide();
            pbxHang4.Hide();
            pbxHang5.Hide();
            pbxHang6.Hide();
            pbxHang7.Hide();

            String file = "words.txt"; 
            StreamReader reader = new StreamReader(file);

            String line;

            //Stores words for each level 
            while ((line = reader.ReadLine()) != null)
            {
                if (line.Length == 3)
                {
                    level1.Add(line);
                }
                if (line.Length == 4)
                {
                    level2.Add(line);
                }             
                if (line.Length == 5)
                {
                    level3.Add(line);
                }
            }
            reader.Close();

            //Gets amount of random numbers based on the number of number of questions in each level
            if (number == 1) // Level 1
            {
                ranNumbers = GetRandomNumbers(level1.Count);
            }
            else if (number == 2) // Level 2
            {
                ranNumbers = GetRandomNumbers(level2.Count);
            }
            else // Level 3
            {
                ranNumbers = GetRandomNumbers(level3.Count);
            }

            // Calculates the first question to be asked
            calculate();              
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            bool turn = false;

            // If there are no more questions to be asked the user will be informed and the form will close
            if (number == 1)
            {
                if (level1.Count < questionCount)
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close();
                }
            }
            else if (number == 2)
            {
                if (level2.Count < questionCount)
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close();
                }
            }
            else
            {
                if (level3.Count < questionCount)
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close();
                }
            }
       
            // if input matches the first letter of the word
            if (txtAnswer.Text == letters[0])
            {
                lblOne.Text = letters[0];
                turn = true; // User responded correctly
            }
            // if input matches the second letter of the word
            if (txtAnswer.Text == letters[1])
            {
                lblTwo.Text = letters[1];
                turn = true; // User responded correctly
            }
            // if input matches the third letter of the word
            if (txtAnswer.Text == letters[2])
            {
                lblThree.Text = letters[2];
                turn = true; // User responded correctly
            }

            if (number == 1) // For level 1
            {
                // User has guessed all letters correctly
                if (lblOne.Text == letters[0] && lblTwo.Text == letters[1] && lblThree.Text == letters[2])
                {
                    MessageBox.Show("Congratuations\nNEXT ");                    
                    next(); // Go to the next question
                }
            }

            else if (number == 2)
            {
                // if input matches the fourth letter of the word
                if (txtAnswer.Text == letters[3])
                {
                    lblFour.Text = letters[3];
                    turn = true; // User responded correctly
                }
                // User has guessed all letters correctly
                if (lblOne.Text == letters[0] && lblTwo.Text == letters[1] && lblThree.Text == letters[2] && lblFour.Text == letters[3])
                {
                    MessageBox.Show("Congratuations\nNEXT ");                   
                    next(); // Go to the next question
                }
            }
            else
            {
                // if input matches the fourth letter of the word
                if (txtAnswer.Text == letters[3])
                {
                    lblFour.Text = letters[3];
                    turn = true; // User responded correctly
                }
                // if input matches the fifth letter of the word
                if (txtAnswer.Text == letters[4])
                {
                    lblFive.Text = letters[4];
                    turn = true; // User responded correctly
                }
                // User has guessed all letters correctly
                if (lblOne.Text == letters[0] && lblTwo.Text == letters[1] && lblThree.Text == letters[2] && lblFour.Text == letters[3] && lblFive.Text == letters[4])
                {
                    MessageBox.Show("Congratuations\nNEXT ");                   
                    next(); // Go to the next question
                }
            }

            // if the user did not respond correctly
            if (turn == false)
            {              
                lbxUsed.Items.Add(txtAnswer.Text); // Add incorrect letter to the list of incorrect guesses
                wrongCount++;
                hang(wrongCount); // Dislpay hangman based on incorrcet guesses

                if (wrongCount == 3) // Read out the word again beacuse the user was incorrect 3 times 
                {
                    if (number == 1)
                        synth.Speak(level1[ranNumbers[nextCount]]);

                    else if (number == 2)
                        synth.Speak(level2[ranNumbers[nextCount]]);

                    else
                        synth.Speak(level3[ranNumbers[nextCount]]);
                }
                // User has failed and is out of guesses
                if (wrongCount > 6)
                {
                    if (number ==1) // level 1
                    {
                        MessageBox.Show("You have failed\nThe answer was: " + level1[ranNumbers[nextCount]] + "\nNEXT ");
                        next(); // Go to the next question
                    }
                    else if (number == 2) // level 2
                    {
                        MessageBox.Show("You have failed\nThe answer was: " + level2[ranNumbers[nextCount]] + "\nNEXT ");
                        next(); // Go to the next question
                    }
                    else
                    {
                        MessageBox.Show("You have failed\nThe answer was: " + level3[ranNumbers[nextCount]] + "\nNEXT ");
                        next(); // Go to the next question
                    }
                }
            }
            txtAnswer.Clear();                     
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            next(); // Go to the next question
        }

        private void next()
        {        
            // Reset to default
            txtAnswer.Clear();
            nextCount++;
            letters.Clear();
            lbxUsed.Items.Clear();
            wrongCount = 0;
            lblOne.Text = "_";
            lblTwo.Text = "_";
            lblThree.Text = "_";
            lblFour.Text = "_";
            lblFive.Text = "_";

            // Hide all the hangman pictures
            pbxHang1.Hide();
            pbxHang2.Hide();
            pbxHang3.Hide();
            pbxHang4.Hide();
            pbxHang5.Hide();
            pbxHang6.Hide();
            pbxHang7.Hide();
              
            calculate(); // Calculates next question to be asked                                                           
        }

        private void hang(int wrongCount)
        {   
            // Show the picture of the hangman based on how many the user has got wrong
            
            if (wrongCount == 1)
            {
                pbxHang1.Show();
            }
            if (wrongCount == 2)
            {
                pbxHang2.Show();
            }
            if (wrongCount == 3)
            {
                pbxHang3.Show();
            }
            if (wrongCount == 4)
            {
                pbxHang4.Show();
            }
            if (wrongCount == 5)
            {
                pbxHang5.Show();
            }
            if (wrongCount == 6)
            {
                pbxHang6.Show();
            }
            if (wrongCount == 7)
            {
                pbxHang7.Show();
            }
        }

        private void calculate()
        {
            questionCount++; // Increment to next question

            if (number == 1) // Level 1
            {
                // Hides label as not used
                lblFive.Hide();
                lblFour.Hide();

                try
                {
                    // Calculates letters from the question word
                    letters.Add(level1[ranNumbers[nextCount]].Substring(0, 1));
                    letters.Add(level1[ranNumbers[nextCount]].Substring(1, 1));
                    letters.Add(level1[ranNumbers[nextCount]].Substring(2, 1));
                    if (paintCount >= 1) // Speak word after paint has been used to speak
                        synth.Speak(level1[ranNumbers[nextCount]]);
                }
                catch (Exception e) // No more questions
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close(); // Close form
                }                   
            }
            else if (number == 2) // Level 2
            {
                lblFive.Hide(); // Hides label as not used

                try
                {
                    // Calculates letters from the question word
                    letters.Add(level2[ranNumbers[nextCount]].Substring(0, 1));
                    letters.Add(level2[ranNumbers[nextCount]].Substring(1, 1));
                    letters.Add(level2[ranNumbers[nextCount]].Substring(2, 1));
                    letters.Add(level2[ranNumbers[nextCount]].Substring(3, 1));
                    if (paintCount >= 1) // Speak word
                        synth.Speak(level2[ranNumbers[nextCount]]);
                }
                catch (Exception e) // No more questions
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close(); // Close form
                }
            }
            else // Level 3
            {
                try
                {
                    // Calculates letters from the question word
                    letters.Add(level3[ranNumbers[nextCount]].Substring(0, 1));
                    letters.Add(level3[ranNumbers[nextCount]].Substring(1, 1));
                    letters.Add(level3[ranNumbers[nextCount]].Substring(2, 1));
                    letters.Add(level3[ranNumbers[nextCount]].Substring(3, 1));
                    letters.Add(level3[ranNumbers[nextCount]].Substring(4, 1));
                    if (paintCount >= 1) // Speak word
                        synth.Speak(level3[ranNumbers[nextCount]]);
                }
                catch (Exception e) // No more questions
                {
                    MessageBox.Show("There are no more questions to answer in this level.\nPlease select another level or quit");
                    Close(); // Close form
                }
            }                     
        }
        // Random number calculator
        public static List<int> GetRandomNumbers(int count)
        {
            // List of random numbers
            Random random = new Random();
            List<int> randomNumbers = new List<int>();

            // Calculate numbers for the amount of questions
            for (int counter = 0; counter < count; counter++)
            {
                // Checks number calculated is not in list of random numners so that each number is unique
                int temp;
                do temp = random.Next(count);
                while (randomNumbers.Contains(temp));
                randomNumbers.Add(temp);
            }
            return randomNumbers; // list
        }

        private void txtAnswer_KeyDown(object sender, KeyEventArgs e)
        {
            // Does not allow numbers to be entered
            if (e.KeyValue>=48 && e.KeyValue<=57)
            {
                e.SuppressKeyPress = true;
            }
        }

        private void Hangman_Paint(object sender, PaintEventArgs e)
        {
            // Reads out the word for the user to guess at the start after the form has opened
            if (paintCount < 1)
            {
                paintCount++;
                if (number == 1) // Level 1
                    synth.Speak(level1[ranNumbers[nextCount]]);
                else if (number == 2) // Level 2
                    synth.Speak(level2[ranNumbers[nextCount]]);
                else // Level 3
                    synth.Speak(level3[ranNumbers[nextCount]]);              
            }
        }

        private void btnHangHelp_Click(object sender, EventArgs e)
        {   
            // Display help form
            Form Help = new Help();
            Help.ShowDialog();
        }
    }
}
